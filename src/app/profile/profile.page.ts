import { ToastController, LoadingController } from '@ionic/angular';
import { Route, Router } from '@angular/router';
import { ServiceUser } from './../service/servise';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  user: any;
  id: any;
  tel: any;
  nom: string;
  prenom: string;
  email: string
  data: any = {};
  mail: string;
  name: string
  constructor(private service: ServiceUser, private toastController: ToastController, private loadingController: LoadingController) { }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: "S'il vous plaît, attendez...",
      spinner: 'bubbles',
    });
    loading.present();
    this.id = localStorage.getItem('token');
    await this.service.user_by_id(this.id).toPromise().then((data: any) => {
      this.data = data.data[0];
    });
    loading.dismiss();
  }


  async update() {
    const loading = await this.loadingController.create({
      message: "S'il vous plaît, attendez...",
      spinner: 'bubbles',
    });
    loading.present();
    if (this.name == this.data.name && this.mail == this.data.email) {
      const toast = await this.toastController.create({
        message: 'Aucune changement.',
        duration: 2000
      });
      toast.present();
    } else {
      try{
        await this.service.update(this.data.id, this.mail, this.name).toPromise().then(async (data: any) => {
          if(data.api_status == 1){
            const toast = await this.toastController.create({
              message: 'Changement réussite.',
              duration: 2000
            });
            await this.service.user_by_id(this.id).toPromise().then((data: any) => {
              this.data = data.data[0];
            });
            toast.present();
          }else {
            const toast = await this.toastController.create({
              message: 'Erreur.',
              duration: 2000
            });
            toast.present();
          }
        })
      }
      catch{
        const toast = await this.toastController.create({
          message: 'Erreur.',
          duration: 2000
        });
        toast.present();
      }
      
    }
    loading.dismiss();
  }

}