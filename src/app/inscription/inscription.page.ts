import { Component, OnInit } from '@angular/core';
import { ServiceUser } from '../service/servise';
import { ToastController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit {
  tel: number;
  mat: string;
  data: any;
  x: any;
  constructor(private service: ServiceUser  ,private rout: NavController, private toastController : ToastController) { }
  ngOnInit() {
  }
  async inscription() {
    
  


  

  if (this.mat>="" && this.tel>=9999999 && this.tel<=99999999){
 
  
      await this.service.add_client(this.tel).toPromise().then((data: any) => {
        console.log(data);
        this.data = data.id;
      });
    await this.service.add_vehicule( this.mat , this.data ).toPromise().then((data: any) => {
      console.log(data);
      
    })
    this.rout.navigateRoot("/home" )
  
 
  
}
    else{
    const toast = await this.toastController.create({
      message: 'donner votre numero et matricule de voiture',
      duration: 2000
    });
    toast.present();
  }

  

}
}