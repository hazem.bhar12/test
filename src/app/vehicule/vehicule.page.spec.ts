import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiculePage } from './vehicule.page';

describe('VehiculePage', () => {
  let component: VehiculePage;
  let fixture: ComponentFixture<VehiculePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehiculePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiculePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
