import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController, LoadingController } from '@ionic/angular';
import { ServiceUser } from './../service/servise';

@Component({
  selector: 'app-vehicule',
  templateUrl: './vehicule.page.html',
  styleUrls: ['./vehicule.page.scss'],
})
export class VehiculePage implements OnInit {
  
  id: any;
  data: any= [{}];
  user: any;
  model: any;
  couleur: any;
  id_client: any;

  constructor(private service: ServiceUser ,private toastController: ToastController, private loadingController: LoadingController) { }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      spinner: 'bubbles',
    });
    loading.present();
    
     

    this.id = localStorage.getItem('token');
    
      await this.service.user_by_id(this.id).toPromise().then((data:any)=>{
        this.user = data.data[0];
      });
      await this.service.vehicule_by_id(this.user.id).toPromise().then((data: any) => {
      this.data = data.data;
    });
    loading.dismiss();
    
  }
  async update(i ) {
  
    const loading = await this.loadingController.create({
      message: "S'il vous plaît, attendez...",
      spinner: 'bubbles',
    });
    loading.present();
    if (this.data[i].model == "" && this.data[i].couleur == "") {
      const toast = await this.toastController.create({
        message: 'Aucune changement.',
        duration: 2000
      });
      toast.present();
    } else {
      try{
        await this.service.update_v(this.data[i].id.toString(),this.data[i].model , this.data[i].couleur ).toPromise().then(async (data: any) => {
          if(data.api_status == 1){
            const toast = await this.toastController.create({
              message: 'Changement réussite.',
              duration: 2000
            });
            await this.service.vehicule_by_id(this.user.id).toPromise().then((data: any) => {
              this.data = data.data;
            });
            toast.present();
          }else {
            const toast = await this.toastController.create({
              message: 'Erreur.',
              duration: 2000
            });
            toast.present();
          }
        })
      }
      catch{
        const toast = await this.toastController.create({
          message: 'Erreur.',
          duration: 2000
        });
        toast.present();
      }
      
    }
    loading.dismiss();
  }
  
}
    
 


