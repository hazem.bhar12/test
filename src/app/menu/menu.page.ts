import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  constructor(private out: AuthService) { }

  ngOnInit() {
  }
  async logout(){
await this.out.logout();
  }

  close(){
    const menu = document.querySelector('ion-menu-controller');
    menu.close();
  }

}
