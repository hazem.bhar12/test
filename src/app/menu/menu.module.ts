import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';
import { AuthGuardService } from '../service/auth_guard';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      { path: 'reservation',  loadChildren: '../reservation/reservation.module#ReservationPageModule' },
      { path: 'profile', loadChildren: '../profile/profile.module#ProfilePageModule' },
      { path: 'facture/:id', loadChildren: '../facture/facture.module#FacturePageModule' },
      { path: 'vehicule', loadChildren: '../vehicule/vehicule.module#VehiculePageModule' },
      { path: 'historique', loadChildren: '../historique/historique.module#HistoriquePageModule' },
      { path: 'map', loadChildren: '../map/map.module#MapPageModule' },

    ]
  },
  {
    path: '',
    redirectTo:'/reservation',
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
