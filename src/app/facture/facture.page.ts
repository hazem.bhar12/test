import { ServiceUser } from './../service/servise';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Screenshot } from '@ionic-native/screenshot/ngx';

@Component({
  selector: 'app-facture',
  templateUrl: './facture.page.html',
  styleUrls: ['./facture.page.scss'],
})
export class FacturePage implements OnInit {
  date: Date ;
  fact: any;
  facture: any = {};
  constructor( private route: ActivatedRoute, private service: ServiceUser, private screenshot: Screenshot ){}
  async ngOnInit(){
   this.date = new Date();
   this.route.params.pipe().subscribe(params => {
    this.fact = params ;
   });
await this.service.facture_by_id(this.fact.id).toPromise().then((data:any)=>{
  this.facture = data;
});

  }
  fun(){
    this.screenshot.save('jpg', 80, 'myscreenshot.jpg').then( onError =>{
      console.log(onError) ;
    });

  }
  }

  


