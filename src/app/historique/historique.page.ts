import { Component, OnInit } from '@angular/core';
import { ServiceUser } from '../service/servise';
import { Router } from '@angular/router';
import { ToastController, LoadingController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-historique',
  templateUrl: './historique.page.html',
  styleUrls: ['./historique.page.scss'],
})
export class HistoriquePage implements OnInit {
  id: any;
  data: any= [{}];
  user: any;

  constructor(private service: ServiceUser ,private rout:Router ,private toastController: ToastController, private loadingController: LoadingController) { }

  async ngOnInit() {
    
    
     

    this.id = localStorage.getItem('token');

    const loadingElement = await this.loadingController.create({
      message: "S'il vous plaît, attendez...",
      spinner: 'bubbles',
    });
    loadingElement.present();
    
      await this.service.user_by_id(this.id).toPromise().then((data:any)=>{
        this.user = data.data[0];
      });
      await this.service.get_facture(this.user.id).toPromise().then((data: any) => {
      this.data = data.data;
    });
   
    loadingElement.dismiss();
  }
  
}
