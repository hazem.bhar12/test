import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Profile } from 'selenium-webdriver/firefox';

@Injectable({ providedIn: 'root' })

export class ServiceUser {
    url = 'http://localhost/project/public/index.php/api/';
    id: any;

    constructor(private http: HttpClient) {
    }

    get_user(){
        return this.http.get(this.url + "get_user") ;
    }
    vehi_by_mat_id(mat:any , id:any){
        return this.http.get(this.url +"vehi_by_mat_id?mat="+mat+ "&id_client="+id)
    }
    user_by_id(id:any){
        return this.http.get(this.url + "user_by_id?num_tel=" + id) ;
    }
    login(tel: number ){
        return this.http.get(this.url + "login?num_tel=" + tel )
    }
    add_client(num_tel: number){
        return this.http.post(this.url + "add_client", 
        {num_tel: num_tel
        }
        )
    }
    add_vehicule(mat : string,id_client: any){
        return this.http.post(this.url + "add_vehicule",
        {mat : mat ,
         id_client: id_client
        }
        )
    }
    ville(){
        return this.http.get(this.url + "ville");
    }
    mun(id_ville:number){
        return this.http.get(this.url + "mun?id_ville=" + id_ville );
    }
    zone(id_mun : number){
        return this.http.get(this.url + "get_zone?id_municipalite=" + id_mun);
    }
    parking(id_zone){
        return this.http.get(this.url + "parking?id_zone=" + id_zone);
    }
    update( id: any, email :any , name:any){
        return this.http.post(this.url + "update_user",
        {   id: id,
            email :email ,
            name:name
         });
    }
    update_v(id:any ,model:any, couleur:any   ){
        return this.http.post(this.url + "update_vehicule",
        {id :id,
            model :model ,
            couleur : couleur,
            

        });
    }
    facture( matricule :any ,date:any , tmp_entrer : any , tmp_sortir : any , prix: any, id_parking:any , id_client : any){
        return this.http.post(this.url + "facture",
        {
            matricule : matricule,
            date : date,
            tmp_entrer : tmp_entrer,
            tmp_sortir : tmp_sortir,
            prix : prix,
            id_parking : id_parking,
            id_client : id_client
        });
    }
    park_by_id(id:any){
        return this.http.get(this.url + "prk_by_id?id=" + id)
    }
    vehicule_by_id(id:any ){
        return this.http.get(this.url + "vehicule_by_id?id_client=" + id)
    }
    solde(id:any , solde: any){
        return this.http.get(this.url + "solde?id=" + id + "&solde=" + solde) ;
    }
    facture_by_id (id:any){
        return this.http.get(this.url + 'facture_by_id?id=' + id )
    }
    get_facture (id:any){
        return this.http.get(this.url + "get_facture?id_client=" + id )
    }
}