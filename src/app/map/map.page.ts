import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import {
  GoogleMaps,
  GoogleMap,
  ILatLng,
  Marker,
} from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation/ngx';


@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

  map: GoogleMap;
  loading: any;
  latlng: ILatLng;

  constructor(private platform: Platform, private geolocation: Geolocation) { }

  async ngOnInit() {

    await this.platform.ready();
    await this.localisation();
}


localisation(){
  this.geolocation.getCurrentPosition().then((resp) => {
    // resp.coords.latitude
    // resp.coords.longitude
    console.log(resp) ;
    this.latlng = { "lat": resp.coords.latitude , "lng": resp.coords.longitude } ;
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        
        target: {
          lat: resp.coords.latitude,
          lng: resp.coords.longitude
        },
        zoom: 18,
        tilt: 30
      }
      
    });
  const marker : Marker =this.map.addMarkerSync({
      icon: 'red',
      animation: 'DROP',
      position: this.latlng 
    });
   }).catch((error) => {
   });
   
}



}