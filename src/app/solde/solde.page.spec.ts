import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoldePage } from './solde.page';

describe('SoldePage', () => {
  let component: SoldePage;
  let fixture: ComponentFixture<SoldePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoldePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoldePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
