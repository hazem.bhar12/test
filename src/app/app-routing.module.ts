import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginService } from './service/login';
import { AuthGuardService } from './service/auth_guard';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home',
  canActivate: [LoginService] ,
   loadChildren: './home/home.module#HomePageModule' },
  { path: 'reservations', 
  loadChildren: './reservation/reservation.module#ReservationPageModule' ,
  canActivate: [AuthGuardService] ,},
  { path: 'inscription', loadChildren: './inscription/inscription.module#InscriptionPageModule' },
  { path: 'menu', loadChildren: './menu/menu.module#MenuPageModule' },
  { path: 'solde', loadChildren: './solde/solde.module#SoldePageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
