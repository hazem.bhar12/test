import { Component, OnInit } from '@angular/core';
import { ServiceUser } from '../service/servise';
import { Router } from '@angular/router';
import { ToastController, LoadingController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.page.html',
  styleUrls: ['./reservation.page.scss'],
})

export class ReservationPage implements OnInit {
  ville: any = [];
  mun: any = [];
  zone: any = [];
  parki: any;
  parking: any = [];
  temp: any;
  today: string;
  time: string;
  timeS: string;
  prk: any;
  prix: any;
  id: any;
  user: any;
  vehicule: any;
  vehicules: any;
  fac: any;
  data: any;
  
  constructor( private service: ServiceUser, private rout: NavController, private loadingController: LoadingController, private toastController: ToastController) { }

  async ngOnInit() {
    

    const loadingElement = await this.loadingController.create({
      message: "S'il vous plaît, attendez...",
      spinner: 'bubbles',
    });
    loadingElement.present();

    this.id = localStorage.getItem('token');
    
    await this.service.user_by_id(this.id).toPromise().then((data:any)=>{
      this.user = data.data[0];
    });
    await this.service.vehicule_by_id(this.user.id).toPromise().then((data: any) => {
    this.vehicules = data.data;
  });
    await this.service.ville().toPromise().then((data: any) => this.ville = data.data);
    loadingElement.dismiss();

  }
  async muni(id: any) {
    await this.service.mun(id.target.value).toPromise().then((data: any) => this.mun = data.data);
  }
  async nzone(id: any) {
    
    await this.service.zone(id.target.value).toPromise().then((data: any) => { this.zone = data.data; });
  }
  async park(id: any) {
    await this.service.parking(id.target.value).toPromise().then((data: any) => this.parking = data.data);
  }
  select(event) {
   
    
  }
  async fact() {
    const loadingElement = await this.loadingController.create({
      message: "S'il vous plaît, attendez...",
      spinner: 'crescent',
    });
    loadingElement.present();
    if (this.vehicule == undefined) {
      const toast = await this.toastController.create({
        message: 'veuillez sélectionner une vehicule.',
        duration: 2000
      });
      toast.present();
    }
    else if (this.parki == undefined) {
      const toast = await this.toastController.create({
        message: 'veuillez sélectionner un parking.',
        duration: 2000
      });
      toast.present();
    }
    else if (this.temp == undefined) {
      const toast = await this.toastController.create({
        message: 'veuillez choisir le temps.',
        duration: 2000
      });
      toast.present();
      
    }
    else {
      const date = new Date();
      var dd1 = date.getDate();
      var mm1 = date.getMonth() + 1;
      var yyyy1 = date.getFullYear();
      this.today = yyyy1 + '/' + mm1 + '/' + dd1 ; 
  
      var hours = date.getHours();
      var minute = date.getMinutes();
      this.time = hours + ':' + minute ;
      const v = date.getMinutes() ;
      date.setTime( date.getTime() + this.temp * 60000 );
      
      var hoursS = date.getHours();
      var minuteS = date.getMinutes();
      this.timeS = hoursS + ':' + minuteS ;
      
      await this.service.park_by_id(this.parki).toPromise().then((data:any)=> {
        
        this.prk = data.data[0];
      });
      
      if ( this.temp == 30 ){ this.prix =this.prk.prix / 2 ; 
      } else if (this.temp == 45 ){ this.prix = (this.prk.prix /4) * 3 
      } else if (this.temp == 60 ){ this.prix = this.prk.prix 
      } else if (this.temp == 90 ){ this.prix = this.prk.prix + (this.prk.prix / 2)
      } else if (this.temp == 120 ){ this.prix = this.prk.prix * 2 }
      if( this.user.solde < this.prix ) {
        const toast = await this.toastController.create({
          message: 'solde insuffisant.',
          duration: 2000
        });
        toast.present();
      }
    else {
      const solde = this.user.solde - this.prix 
      await this.service.solde(this.user.id , solde ).toPromise().then(data=>{
       
      })
      await this.service.facture(this.vehicule ,this.today , this.time , this.timeS , this.prix.toString()  , this.parki , this.user.id ).toPromise().then((data:any)=>{
        this.fac = data ;
        
      });
      this.rout.navigateRoot("/menu/facture/" + this.fac.id) ;
    }
     
    }
    loadingElement.dismiss();

  }


}
