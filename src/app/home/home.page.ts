import { Component } from '@angular/core';
import { ServiceUser } from '../service/servise';
import { Route, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  user: any;
  tel: any;
  mat:any
  vehi: any;
  uuser: any;
  constructor( private service: ServiceUser , public rout: Router, private toastController: ToastController,private loadingController: LoadingController){

  }
  async ngOnInit(){
    await this.get_user();
  }

  async get_user(){
   await this.service.get_user().toPromise().then( (data:any)=>{
    
this.user = data.data ;

    })

  }
  async login(){
    
    if(this.tel>=""&& this.mat>=""){
      const loadingElement = await this.loadingController.create({
        message: 'Please wait...',
        spinner: 'bubbles',
      });
      loadingElement.present();
    await this.service.login(this.tel).toPromise().then(async (data:any)=>{
      
      if(data.api_message ==='success') {
        await this.service.user_by_id(this.tel).toPromise().then( (data:any)=>{
          this.uuser=data.data[0];
        })
        await this.service.vehi_by_mat_id(this.mat,this.uuser.id).toPromise().then( async (data:any)=>{
                  
          if(data.api_message === 'success' ){
            this.vehi=data.data[0];

        localStorage.setItem('isLoggedIn' , 'true') ;
        localStorage.setItem('token' ,  this.tel) ;
        this.rout.navigateByUrl('/menu/reservation') ;  
      } 
      else{
        const toast = await this.toastController.create({
          message: 'donner une des votres matricule',
          duration: 2000
        });
        toast.present();
      
  
      }
    
    
  })
      }
  
      else {
        const toast = await this.toastController.create({
          message: 'numero incorrect.',
          duration: 2000
        });
        toast.present();
      }
    
    });
    loadingElement.dismiss();

  }
  else{
    const toast = await this.toastController.create({
      message: 'composer votre numero et une matricule de vehicule.',
      duration: 2000
    });
    toast.present();

  }
  }

}
